import * as actionTypes from "../../action-types";
import * as mutationTypes from "../../mutation-types";

import service from "../../../services/user";

export default {
    async [actionTypes.LOG_IN]({ commit }, user) {
        const userName = user.email;
        const { data } = await service.user(user);
        if (data === 200) {
            commit(mutationTypes.LOG_IN_MUTATION, userName);
        } else {
            commit(mutationTypes.LOG_IN_MUTATION_FAILED, "Error");
        }
    },

    async [actionTypes.SET_MESSAGE]({ commit }, message) {
        commit(mutationTypes.SET_MESSAGE_MUTATION, message);
    }
};