import * as mutationTypes from "../../mutation-types";

export default {
    [mutationTypes.LOG_IN_MUTATION](state, username) {
        let { authenticated } = state;
        authenticated = true;

        state.authenticated = authenticated;

        localStorage.email = username;
    },

    [mutationTypes.LOG_IN_MUTATION_FAILED](state, _data) {
        let { message } = state;
        message = _data;
        authenticated = null;

        state.authenticated = authenticated;
        state.message = message;

        localStorage.removeItem("email");
    },
};