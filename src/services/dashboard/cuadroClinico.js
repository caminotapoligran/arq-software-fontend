import axios from "@/config/api";

export default async(data) => {
    return axios
        .post("/crearCuadroClinico", data)
        .then((response) => {
            if (response) {
                return response;
            } else {
                return { data: { message: "Ha ocurrido un error" } };
            }
        })
        .catch((error) => {
            console.log(error);
        });
};