import axios from "@/config/api";

export default async(state) => {
    return axios
        .post("/buscarPorEstado", { data: state })
        .then((response) => {
            if (response) {
                return response;
            } else {
                return { data: { message: "Ha ocurrido un error" } };
            }
        })
        .catch((error) => {
            console.log(error);
        });
};