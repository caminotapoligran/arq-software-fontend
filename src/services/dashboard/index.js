import buscarEstado from './buscarEstado';
import cuadroClinico from './cuadroClinico';
import getAllColaborador from './getAllColaborador';


export default {
    buscarEstado,
    cuadroClinico,
    getAllColaborador
};