import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import 'flowbite';
import './index.css'

import storageLocalMixin from "./mixins/storageLocal";

Vue.mixin(storageLocalMixin);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");